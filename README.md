# Snakemake workflow for Syri

**Table of Contents**
 
  - [Description](#description)
  - [Dependencies](#dependencies)
  - [Overview of workflow](#overview-of-workflow)
  - [Procedure](#procedure)
  - [Output](#output)
  - [License](#License)



## Description
This workflow will compare the genomesprovided using syri. It will process all the permutations and for each permutation it will provide a result using nucmer and minimap2 as an aligner

## Dependencies
This worfkflow is based on Snakemake and singularity. It will therefore download every image needed to work properly. Therefore the dependencies are:
Snakemake
singularity (apptainer)



## Overview of workflow
<p align="center">
<img src="images/dag.png" width="50%">
</p>


## Procedure

- Clone the code repository from github to your computer by running the
  following shell command in your terminal:
```bash
git clone https://gitlab.cirad.fr/umr_agap/workflows/syri_combination.git
```


- Change directories into the cloned repository:

```bash
cd syri_combination
```

- Adapt the config.yaml file to provide the genome you want to compare

- Launch the workflow
```bash
sbatch snakemake.sh
```


## Output
Images for all combination

## License
MIT License

## Author

Gautier Sarah - INRAE

## ToDo

- [ ] Add a drawing with all the genomes in the same image in the order they are provided
- [ ] Add the possibility to select only one aligner
- [ ] Add the possibility to select only intra or interchromosomal drawing
- [ ] Add the possibility to change different parameters in each software used

