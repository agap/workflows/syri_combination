from snakemake.utils import min_version
from os.path import basename
from itertools import permutations

min_version("7.00.0")

#This workflow aims to compare haplotype from a single genome and detect hemizygous part of it
input_genomes = config["input_genomes"]

list_genomes = input_genomes.keys()

list_permutations = ['_on_'.join(pair) for pair in permutations(list_genomes, 2)]

wildcard_constraints:
    aligner = "|".join(["minimap2","nucmer"])

def get_reference(wildcards):
    return input_genomes[wildcards.reference]


def get_query(wildcards):
    return input_genomes[wildcards.query]

rule all:
    input:
        expand("results/plots_minimap2/PLOT_interchro_minimap2_{permutation}.png",permutation=list_permutations),
        expand("results/plots_nucmer/PLOT_interchro_nucmer_{permutation}.png",permutation=list_permutations),
        expand("results/plots_minimap2/PLOT_intrachro_minimap2_{permutation}.png",permutation=list_permutations),
        expand("results/plots_nucmer/PLOT_intrachro_nucmer_{permutation}.png",permutation=list_permutations)

rule nucmer:
    input:
        reference=get_reference,
        query=get_query
    output:
        "results/nucmer/{query}_on_{reference}.delta"
    threads:1
    container:config["container"]["mummer"]
    shell:
        "nucmer {input.reference} {input.query} --delta {output} -l 100 -c 500 -b 500 -t {threads}"


rule deltafilter:
    input:
        "results/nucmer/{query}_on_{reference}.delta"
    output:
        temp("results/nucmer/{query}_on_{reference}.filtered.delta")
    threads:1
    container:config["container"]["mummer"]
    shell:
        "delta-filter -m -i 90 -l 5000 {input} > {output}"

rule showcoords:
    input:
        "results/nucmer/{query}_on_{reference}.filtered.delta"
    output:
        temp("results/nucmer/{query}_on_{reference}.coords")
    threads:1
    container: config["container"]["mummer"]
    shell:
        "show-coords -THrd {input} >{output}"

rule syri_nucmer:
    input:
        coords="results/nucmer/{query}_on_{reference}.coords",
        delta="results/nucmer/{query}_on_{reference}.filtered.delta",
        reference=get_reference,
        query=get_query
    output:
        syri="results/syri/nucmer_{query}_on_{reference}_syri.out"
    threads:1
    container:config["container"]["syri"]
    shell:"""
        syri -c {input.coords} -d {input.delta} -r {input.reference} -q {input.query} --nc {threads} --nosnp --dir results/nucmer --prefix nucmer_{wildcards.query}_on_{wildcards.reference}_
        mv results/nucmer/nucmer_{wildcards.query}_on_{wildcards.reference}_* results/syri
        """

rule minimap2:
    input:
        reference=get_reference,
        query=get_query
    output:
        sam=temp("results/minimap2/{query}_on_{reference}.sam")
    threads:1
    container: config["container"]["minimap2"]
    shell:
        "minimap2 -a --eqx -x asm5 -t {threads} {input.reference} {input.query} >{output.sam}"

rule samtools_sort:
    input:
        sam="{file}.sam"
    output:
        bam="{file}.bam"
    threads:1
    container: config["container"]["samtools"]
    shell:
        "samtools sort -O BAM -@ {threads} {input.sam} >{output.bam}"

rule create_genome_txt:
    input:
        reference=get_reference,
        query=get_query
    output:
        "results/{query}_on_{reference}_genomes.txt"
    threads:1
    shell:"""
        echo -e "{input.reference}\t{wildcards.reference}\tlw:1.5" >{output}
        echo -e "{input.query}\t{wildcards.query}\tlw:1.5" >>{output}
    """

rule syri_minimap2:
    input:
        alignment="results/{aligner}/{query}_on_{reference}.sam",
        reference=get_reference,
        query=get_query
    output:
        syri="results/syri/{aligner}_{query}_on_{reference}_syri.out"
    threads:1
    container:config["container"]["syri"]
    shell:"""
        syri -c {input.alignment} -r {input.reference} -q {input.query} --nc {threads} -F S --dir results/{wildcards.aligner} --prefix {wildcards.aligner}_{wildcards.query}_on_{wildcards.reference}_
        mv results/{wildcards.aligner}/{wildcards.aligner}_{wildcards.query}_on_{wildcards.reference}_* results/syri
        """

rule plotsr_interchromosome:
    input:
        syri="results/syri/{aligner}_{query}_on_{reference}_syri.out",
        genomes="results/{query}_on_{reference}_genomes.txt"
    output:
        interchro ="results/plots_{aligner}/PLOT_interchro_{aligner}_{query}_on_{reference}.png",
    threads:1
    container:config["container"]["plotsr"]
    shell:"""
        plotsr --sr {input.syri} --genomes {input.genomes} -o {output.interchro} -v --itx
    """

rule plotsr_intrachromosome:
    input:
        syri="results/syri/{aligner}_{query}_on_{reference}_syri.out",
        genomes="results/{query}_on_{reference}_genomes.txt"
    output:
        intrachro ="results/plots_{aligner}/PLOT_intrachro_{aligner}_{query}_on_{reference}.png"
    threads:1
    container:config["container"]["plotsr"]
    shell:"""
        plotsr --sr {input.syri} --genomes {input.genomes} -o {output.intrachro}
    """
