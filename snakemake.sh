#!/bin/bash 
#
#SBATCH -J Syri
#SBATCH -o Syri."%j".out
#SBATCH -e Syri."%j".err 

#SBATCH -p agap_long

module purge
module load snakemake/7.15.1-conda
module load singularity/3.6.3
 
mkdir -p logs
 

 


# Print shell commands (Mode dry-run)
#snakemake  --profile slurm  --configfile config.yaml  --printshellcmds --dryrun --use-singularity  


# Unlock repository if one job failed
# snakemake  --profile slurm  --configfile config.yaml  --unlock  --use-singularity
 
# Create DAG file
#snakemake  --profile slurm  --configfile config.yaml  --dag  --use-singularity | dot -Tpdf > dag.pdf

# Run the workflow
snakemake --profile slurm  --configfile config.yaml  --use-singularity 